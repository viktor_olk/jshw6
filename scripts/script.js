// Реалізуйте клас Worker (Працівник), який матиме такі властивості: name (ім'я), surname (прізвище),
// rate (ставка за день роботи), days (кількість відпрацьованих днів).
// Також клас повинен мати метод getSalary(), який виводитиме зарплату працівника.
// Зарплата - це добуток (множення) ставки rate на кількість відпрацьованих днів days.
class Worker {
  constructor(name, surname, rate) {
    this.name = name;
    this.surname = surname;
    this.rate = rate;
    this.days = 0;
  }

  resetDays() {
    this.days = 0;
  }

  setDays(count) {
    this.days += count;
  }

  getSalary() {
    let salary = this.rate * this.days;
    document.write(
      `Заробітна плата ${this.surname} ${this.name} становить ${salary} грн.<br>`
    );
  }
}

let workerOne = new Worker("Віктор", "Олійник", 220);
workerOne.setDays(55);
workerOne.getSalary();

// Реалізуйте клас MyString, який матиме такі методи: метод reverse(),
// який параметром приймає рядок, а повертає її в перевернутому вигляді, метод ucFirst(),
// який параметром приймає рядок, а повертає цей же рядок, зробивши його першу літеру великою
// та метод ucWords, який приймає рядок та робить заголовною першу літеру кожного слова цього рядка.

class MyString {
  constructor() {
    this.string = "";
  }

  reverse(str) {
    this.string = str.split(" ").reverse().join(" ");
    document.write(
      `<br>Вихідна строка: ${str}.<br> Перевернута строка: ${this.string}.<br>`
    );
  }

  ucFirst(str) {
    this.string = str[0].toUpperCase() + str.slice(1);
    document.write(`<br>Початок строки з великої літери: ${this.string}<br>`);
  }

  ucWords(str) {
    this.string = str
      .split(" ")
      .map((item) => {
        return item[0].toUpperCase() + item.slice(1);
      })
      .join(" ");
    document.write(
      `<br>Початок кожного слова строки з великої літери: ${this.string}<br>`
    );
  }
}

let stringObj = new MyString();

stringObj.reverse("слово1 слово2 слово3 слово4");
stringObj.ucFirst("слово1 слово2 слово3 слово4");
stringObj.ucWords("слово1 слово2 слово3 слово4");

// Створіть клас Phone, який містить змінні number, model і weight.
// Створіть три екземпляри цього класу.
// Виведіть на консоль значення їх змінних.
// Додати в клас Phone методи: receiveCall, має один параметр - ім'я. Виводить на консоль повідомлення "Телефонує {name}". Метод getNumber повертає номер телефону. Викликати ці методи кожного з об'єктів.

class Phone {
  constructor(number, model, weight) {
    this.number = number;
    this.model = model;
    this.weight = weight;
  }
  receiveCall(name) {
    console.log(`Телефонує ${name}!`);
  }

  getNumber() {
    return `Номер телефону: ${this.number}`;
  }
}

let inst1 = new Phone(12345, "iphone", 0.2);
let inst2 = new Phone(67890, "samsung", 0.18);
let inst3 = new Phone(13570, "lg", 0.21);
console.dir(Object.values(inst1));
console.log(Object.values(inst2));
console.info(Object.values(inst3));

inst1.receiveCall("Гнат");
console.log(inst1.getNumber());

inst2.receiveCall("Василь");
console.log(inst2.getNumber());

inst3.receiveCall("Федір");
console.log(inst3.getNumber());

// Створити клас Car , Engine та Driver.
// Клас Driver містить поля - ПІБ, стаж водіння.
// Клас Engine містить поля – потужність, виробник.
// Клас Car містить поля – марка автомобіля, клас автомобіля, вага, водій типу Driver, мотор типу Engine. Методи start(), stop(), turnRight(), turnLeft(), які виводять на друк: "Поїхали", "Зупиняємося", "Поворот праворуч" або "Поворот ліворуч". А також метод toString(), який виводить повну інформацію про автомобіль, її водія і двигуна.

class Driver {
  constructor(fullName, experience) {
    this.fullName = fullName;
    this.experience = experience;
  }
}

class Engine {
  constructor(power, producer) {
    this.power = power;
    this.producer = producer;
  }
}

class Car {
  constructor(brand, classCar, weight, fullName, experience, power, producer) {
    this.brand = brand;
    this.classCar = classCar;
    this.weight = weight;
    this.driver = new Driver(fullName, experience);
    this.engine = new Engine(power, producer);
  }
  start() {
    document.write(`Поїхали<br>`);
  }
  stop() {
    document.write(`Зупиняємося<br>`);
  }
  turnRight() {
    document.write(`Поворот праворуч<br>`);
  }
  turnLeft() {
    document.write(`Поворот ліворуч<br>`);
  }

  toString() {
    return `<br>Марка авто: ${this.brand}.
      <br>Клас авто: ${this.classCar}-клас.
      <br>Вага авто: ${this.weight} кг.
      <br>Потужність двигуна: ${this.engine.power} кс.
      <br>Виробник двигуна: ${this.engine.producer}.
      <br>ПІБ водія: ${this.driver.fullName}.
      <br>Стаж водіння: ${this.driver.experience} років.<br>`;
  }
}

let firstCar = new Car(
  "Audi",
  "S",
  3000,
  "Українець Петро Петрович",
  12,
  455,
  "VAG"
);
firstCar.start();
firstCar.stop();
firstCar.turnRight();
firstCar.turnLeft();
document.write(firstCar.toString());

// Створити похідний від Car клас - Lorry (вантажівка), що характеризується також вантажопідйомністю кузова.

class Lorry extends Car {
  constructor(
    brand,
    classCar,
    weight,
    fullName,
    experience,
    power,
    producer,
    carrying
  ) {
    super(brand, classCar, weight, fullName, experience, power, producer);
    this.carrying = carrying;
  }
  toString() {
    return `${super.toString()}
      Вантажопідйомність: ${this.carrying} т.<br>`;
  }
}

let secondCar = new Lorry(
  "MAN",
  "TIR",
  8000,
  "Українець Василь Васильович",
  20,
  510,
  "MAN Motors",
  42
);

document.write(secondCar.toString());

// Створити похідний від Car клас - SportCar, який також характеризується граничною швидкістю.

class SportCar extends Car {
  constructor(
    brand,
    classCar,
    weight,
    fullName,
    experience,
    power,
    producer,
    maxSpeed
  ) {
    super(brand, classCar, weight, fullName, experience, power, producer);
    this.maxSpeed = maxSpeed;
  }
  toString() {
    return `${super.toString()}
    Максимальна швидкість: ${this.maxSpeed} км/год.<br>`;
  }
}

let thirdCar = new SportCar(
  "Lamborghini",
  "Sport Tourer",
  1800,
  "Українець Яків Якович",
  18,
  655,
  "VAG",
  330
);

document.write(thirdCar.toString());

// https://storage.googleapis.com/www.examclouds.com/oop/car-ierarchy.png

// Створити клас Animal та розширюючі його класи Dog, Cat, Horse.
// Клас Animal містить змінні food, location і методи makeNoise, eat, sleep. Метод makeNoise, наприклад, може виводити на консоль "Така тварина спить".
// Dog, Cat, Horse перевизначають методи makeNoise, eat.
// Додайте змінні до класів Dog, Cat, Horse, що характеризують лише цих тварин.

class Animal {
  constructor(noise, food, location) {
    this.noise = noise;
    this.food = food;
    this.location = location;
  }
  makeNoise() {
    return `Така тварина видає звуки: ${this.noise}! ${this.noise}!`;
  }
  eat() {
    return `Така тварина їсть ${this.food}.`;
  }
  sleep() {
    return `Така тварина спить`;
  }
}

class Dog extends Animal {
  constructor(noise, food, location, nickname) {
    super(noise, food, location);
    this.nickname = nickname;
  }
  makeNoise() {
    return `Собака видає звук ${super.noise}! ${super.noise}!`;
  }
  eat() {
    return `Собака їсть ${super.food}.`;
  }
}

class Cat extends Animal {
  constructor(noise, food, location, color) {
    super(noise, food, location);
    this.color = color;
  }
  makeNoise() {
    return `Кіт видае звук ${super.noise}!`;
  }

  eat() {
    return `Кіт їсть ${super.food}.`;
  }
}

class Horse extends Animal {
  constructor(noise, food, location, breed) {
    super(noise, food, location);
    this.breed = breed;
  }
  makeNoise() {
    return `Кінь видає звук ${super.noise}!`;
  }
  eat() {
    return `Кінь їсть ${super.food}.`;
  }
}

// Створіть клас Ветеринар, у якому визначте метод void treatAnimal(Animal animal). Нехай цей метод роздруковує food і location тварини, що прийшла на прийом.
// У методі main створіть масив типу Animal, в який запишіть тварин всіх типів, що є у вас. У циклі надсилайте їх на прийом до ветеринара.

class Veterinary {
  treatAnimal(animal) {
    return `<br>Їжа: ${animal.food}.
    <br>Місце знаходження: ${animal.location}.<br>`;
  }
  main(...args) {
    let patientList = ``;
    [...args].map((patient) => {
      patientList += `<br>${patient.constructor.name} на прийом.`;
    });
    return patientList;
  }
}

let dog = new Dog("гав", "кістки", "двір", "Сатурн");
let cat = new Cat("мяу", "миші", "дім", "сірий");
let horse = new Horse("ігого", "овес", "стайня", "рисак");

document.write(new Veterinary().treatAnimal(dog));
document.write(new Veterinary().treatAnimal(cat));
document.write(new Veterinary().treatAnimal(horse));
document.write(new Veterinary().main(dog, cat, horse));
